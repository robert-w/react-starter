import ReactDOM from 'react-dom';
import React from 'react';

import App from 'home/App.js';
import 'home/index.scss';

let launch = function() {
	// Use hydrate when utilizing server side rendering
	let render = process.env.SERVER_SIDE_RENDERING
		? ReactDOM.hydrate
		: ReactDOM.render;

	// Grab any initial state from the server
	let default_state = window.INITIAL_STATE || {};

	render(<App {...default_state} />, document.getElementById('react-root'));
};

if (document.readyState === 'complete') {
	launch();
} else {
	window.onload = launch;
}

// Enable HMR
if (process.env.NODE_ENV === 'development' && module && module.hot) {
	module.hot.accept('./App.js', () => {
		let HotApp = require('./App.js').default;
		ReactDOM.render(<HotApp />, document.getElementById('react-root'));
	});
}
