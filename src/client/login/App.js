import authenticationService from './services/authentication.service.js';
import { createGenerateClassName } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Page from '../shared/components/Page.js';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import React from 'react';

export default class Login extends React.Component {
	constructor(props) {
		super(props);
		this.generateClassName = createGenerateClassName();
		// Create some refs for dom nodes to make getting their values easy
		this.emailRef = React.createRef();
		this.passwordRef = React.createRef();
	}

	signIn = () => {
		let email = this.emailRef.current.value;
		let password = this.passwordRef.current.value;

		authenticationService
			.signin(email, password)
			.then(console.log)
			.catch(console.error);
	};

	register = () => {
		let email = this.emailRef.current.value;
		let password = this.passwordRef.current.value;

		authenticationService
			.register(email, password)
			.then(console.log)
			.catch(console.error);
	};

	render() {
		return (
			<Page
				manager={this.props.manager}
				registry={this.props.registry}
				generateClassName={this.generateClassName}
			>
				<Paper className="login__container">
					<form className="login__form flex-column">
						<TextField
							id="email"
							inputRef={this.emailRef}
							className="login__form__input"
							variant="outlined"
							label="Email"
							margin="normal"
							type="text"
						/>
						<TextField
							id="password"
							autoComplete="password"
							inputRef={this.passwordRef}
							className="login__form__input"
							variant="outlined"
							label="Password"
							margin="normal"
							type="password"
						/>
						<div className="login__actions flex">
							<Button
								variant="contained"
								color="secondary"
								onClick={this.register}
							>
								Sign Up
							</Button>
							<Button
								variant="contained"
								color="secondary"
								onClick={this.signIn}
							>
								Sign In
							</Button>
						</div>
					</form>
				</Paper>
			</Page>
		);
	}
}
