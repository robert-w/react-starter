import superagent from 'superagent';

export default {
	/**
	 * @function signin
	 * @description Attempt to signin a user
	 * @param {String} email - Email address
	 * @param {String} password - Password
	 * @return {Promise<Object, Error>}
	 */
	signin(email, password) {
		return superagent
			.post('/auth/signin')
			.set('Content-type', 'application/x-www-form-urlencoded')
			.send({ username: email })
			.send({ password: password });
	},

	/**
	 * @function register
	 * @description Attempt to register a new user
	 * @param {String} email - Email address
	 * @param {String} password - Password
	 * @return {Promise<Object, Error>}
	 */
	register(email, password) {
		return superagent
			.post('/auth/register')
			.set('Content-type', 'application/x-www-form-urlencoded')
			.send({ username: email })
			.send({ password: password });
	},
};
