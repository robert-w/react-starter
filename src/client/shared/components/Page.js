import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import JssProvider from 'react-jss/lib/JssProvider';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';
import React from 'react';

let theme = createMuiTheme({
	palette: {
		type: 'light',
		primary: indigo,
		secondary: pink,
		error: red,
		tonalOffset: 0.2,
	},
	typography: {
		useNextVariants: true,
	},
});

export default class Page extends React.Component {
	componentDidMount() {
		let ssr = document.getElementById('material-ssr');
		if (ssr && ssr.parentNode) {
			ssr.parentNode.removeChild(ssr);
		}
	}

	render() {
		let { children, generateClassName, registry, manager } = this.props;

		return (
			<JssProvider registry={registry} generateClassName={generateClassName}>
				<MuiThemeProvider sheetsManager={manager} theme={theme}>
					<header className="app__header flex" />
					<div className="app__body flex">{children}</div>
				</MuiThemeProvider>
			</JssProvider>
		);
	}
}
