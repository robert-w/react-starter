export default function asyncAction(api) {
	return next => {
		return action =>
			typeof action === 'function' ? action(api.dispatch) : next(action);
	};
}
