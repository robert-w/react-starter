const STYLE = {
	action: 'font-weight:bold;font-size:1.1em',
	state: 'color:blue;',
};

export default function devLogger(api) {
	return next => {
		return action => {
			let result = next(action);
			console.log(`%c ${action.type}:`, STYLE.action, action);
			console.log('%c $next state', STYLE.state, api.getState());
			return result;
		};
	};
}
