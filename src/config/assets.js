/**
 * @name exports
 * @static
 * @summary location of all assets
 * @description Paths for files relative to src
 */
module.exports = {
	components: 'src/client/**/App.js',
	entries: 'src/client/**/index.js',
	routes: 'src/server/**/*.routes.js',
	styles: 'src/client/**/index.scss',
	views: 'src/server/**/views',
};
