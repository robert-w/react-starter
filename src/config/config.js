const assets = require('./assets');
const path = require('path');
const glob = require('glob');

function mapEntries(filepaths) {
	return filepaths.reduce((all, filepath) => {
		let key = path.posix
			.dirname(filepath)
			.split(path.posix.sep)
			.pop();
		all[key] = filepath;
		return all;
	}, {});
}

function mapAliases(filepaths) {
	return filepaths.reduce((all, filepath) => {
		let alias = path.posix.dirname(filepath);
		let key = alias.split(path.posix.sep).pop();
		all[key] = alias;
		return all;
	}, {});
}

function getFilePaths() {
	return {
		files: {
			components: mapEntries(glob.sync(path.posix.resolve(assets.components))),
			entries: mapEntries(glob.sync(path.posix.resolve(assets.entries))),
			aliases: mapAliases(glob.sync(path.posix.resolve(assets.entries))),
			styles: glob.sync(path.posix.resolve(assets.styles)),
			routes: glob.sync(path.posix.resolve(assets.routes)),
			views: glob.sync(path.posix.resolve(assets.views)),
		},
	};
}

function getClientConfig() {
	return {
		client: {
			bin: path.posix.resolve('./bin'),
			stats: path.posix.resolve('./bin/stats.json'),
			precompiled: path.posix.resolve('./bin/precompiled-stats.json'),
		},
	};
}

function setupConfig() {
	// TODO: Pull this from environment specific files
	// Unsafe-eval is used for development of hot module replacement
	// It is not needed for production, so prod locals won't need that
	let baseConfig = {
		locals: {
			title: 'React-Starter',
			author: 'Robert-W <https://github.com/Robert-W>',
			keywords: 'React, React-Router, Redux',
			description: 'Starterpack for a React powered SPA.',
			contentSecurityPolicy:
				"script-src 'self' 'unsafe-eval';style-src 'self' 'unsafe-inline'",
		},
	};

	return Object.assign({}, baseConfig, getClientConfig(), getFilePaths());
}

module.exports = setupConfig();
