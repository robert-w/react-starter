module.exports = function getEnvironmentSettings() {
	// Define any environment variables we currently have for all projects
	// Eventually, we shall mixin any env variables defined from other sources
	let raw = {
		BABEL_ENV: process.env.NODE_ENV || 'development',
		NODE_ENV: process.env.NODE_ENV || 'development',
		PORT: process.env.PORT || 3000,
		PUBLIC_DIRECTORY: 'public',
		SERVER: true,
	};

	// Stringify any variables that do not get interpreted correctly at runtime
	// for example, 'development' in DefinePlugin turns into a var and will break
	// but a number or boolean is fine to leave alone
	let webpack_env = {
		'process.env': Object.keys(raw).reduce((env, key) => {
			let type = typeof process.env[key];
			let value = process.env[key] || raw[key];
			env[key] =
				type === 'boolean' || type === 'number' ? value : JSON.stringify(value);
			return env;
		}, {}),
	};

	return { raw, webpack_env };
};
