const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const config = require('./config');
const path = require('path');

module.exports = function WebpackDevConfig() {
	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	// Get our entries and aliases
	let { entries, aliases } = config.files;

	// Modify our entries to have hot middleware with them
	let hotEntries = Object.keys(entries).reduce((all, key) => {
		all[key] = ['webpack-hot-middleware/client', entries[key]];
		return all;
	}, {});

	return {
		mode: env.NODE_ENV,
		profile: true,
		entry: hotEntries,
		resolve: {
			alias: aliases,
		},
		output: {
			path: path.posix.join(process.cwd(), env.PUBLIC_DIRECTORY),
			publicPath: `/${env.PUBLIC_DIRECTORY}/`,
			filename: '[name].[hash].js',
		},
		module: {
			rules: [
				{
					test: /\.js?$/,
					loader: 'babel-loader',
					exclude: /(node_modules)/,
				},
				{
					test: /\.scss$/,
					use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
				},
			],
		},
		plugins: [
			new StatsWriterPlugin({
				filename: path.posix.resolve('./bin/stats.json'),
			}),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.HotModuleReplacementPlugin(),
			new webpack.DefinePlugin(webpack_env),
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 2,
						minSize: 0,
					},
				},
			},
		},
	};
};
