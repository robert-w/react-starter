const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const config = require('./config');
const path = require('path');

module.exports = function WebpackPrecompileConfig() {
	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	// Get our entries and aliases
	let { aliases, components } = config.files;

	return {
		mode: env.NODE_ENV,
		profile: true,
		target: 'node',
		entry: components,
		resolve: {
			alias: aliases,
		},
		output: {
			path: path.posix.join(config.client.bin, 'server-components'),
			filename: '[name].compiled.js',
			libraryTarget: 'commonjs2',
		},
		module: {
			rules: [
				{
					test: /\.js?$/,
					loader: 'babel-loader',
					exclude: /(node_modules)/,
				},
				{
					test: /\.scss$/,
					use: ['css-loader', 'postcss-loader', 'sass-loader'],
				},
			],
		},
		plugins: [
			new StatsWriterPlugin({ filename: config.client.precompiled }),
			new webpack.DefinePlugin(webpack_env),
			new webpack.optimize.OccurrenceOrderPlugin(),
		],
	};
};
