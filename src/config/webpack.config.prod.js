const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const config = require('./config');
const path = require('path');

module.exports = function WebpackProdConfig() {
	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	// Get our entries and aliases
	let { entries, aliases, styles } = config.files;

	let excludes = [],
		rules = [];
	// We need to build up the plugins and rules for our sass files
	// the style files here are not all that we have, just ones that
	// need to be extracted and inlined
	styles.forEach(style => {
		let { dir, base } = path.posix.parse(style);
		// The alias used is what we need to match
		let alias = dir.split(path.posix.sep).pop();
		let name = path.posix.join(alias, base);

		let regex = new RegExp(name + '$');
		// Add this to the excludes so it does not get loaded with style-loader
		excludes.push(regex);
		// Add a rule telling webpack to extract this file
		rules.push({
			test: regex,
			use: [
				MiniCssExtractPlugin.loader,
				'css-loader',
				'postcss-loader',
				'sass-loader',
			],
		});
	});

	return {
		mode: env.NODE_ENV,
		entry: entries,
		resolve: {
			alias: aliases,
		},
		output: {
			path: path.posix.join(process.cwd(), 'bin', env.PUBLIC_DIRECTORY),
			publicPath: `/${env.PUBLIC_DIRECTORY}/`,
			filename: '[name].[hash].js',
		},
		module: {
			rules: [
				{
					test: /\.js?$/,
					loader: 'babel-loader',
					exclude: /(node_modules)/,
				},
				{
					test: /\.scss$/,
					use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
					exclude: excludes,
				},
				...rules,
			],
		},
		plugins: [
			new StatsWriterPlugin({ filename: config.client.stats }),
			new MiniCssExtractPlugin({ filename: '[name].[contenthash].css' }),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.DefinePlugin(webpack_env),
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 2,
						minSize: 0,
					},
				},
			},
		},
	};
};
