const winston = require('winston');

let format = winston.format.combine(
	winston.format.timestamp(),
	winston.format.json(),
);

winston.loggers.add('Application', {
	format: format,
	transports: [
		new winston.transports.Console({
			level: 'debug'
		})
	]
});

winston.loggers.add('Audit', {
	format: format,
	transports: [
		new winston.transports.Console({
			level: 'info'
		})
	]
});


/**
 * @name exports
 * @static
 * @summary Logger to use for the application
 */
module.exports = {
	application: winston.loggers.get('Application'),
	audit: winston.loggers.get('Audit'),
};
