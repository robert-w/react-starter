const method_override = require('method-override');
const compression = require('compression');
const body_parser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const path = require('path');

class Server {
	constructor(props) {
		// Mixin any props we need to
		Object.assign(this, props);
		// Create a server instance
		this.app = express();
		// return self for chaining
		return this;
	}

	configureMiddleware() {
		// Enable stack traces
		this.app.set('showStackError', true);
		// Enable jsonp via res.jsonp
		this.app.set('jsonp callback', true);
		// Enable compression
		this.app.use(compression({ level: 9 }));
		// Enable body parser
		this.app.use(body_parser.urlencoded({ extended: true }));
		this.app.use(body_parser.json());
		// Enable method override to allow for put and delete
		this.app.use(method_override());

		return this;
	}

	configureLocals(locals = {}) {
		this.app.locals.title = locals.title;
		this.app.locals.author = locals.author;
		this.app.locals.keyword = locals.keyword;
		this.app.locals.description = locals.description;
		this.app.locals.contentSecurityPolicy = locals.contentSecurityPolicy;

		return this;
	}

	configureSession() {
		return this;
	}

	configurePassport() {
		return this;
	}

	configureViewEngine(engine = 'pug', views = '') {
		this.app.set('view engine', engine);
		this.app.set('views', views);

		return this;
	}

	/**
	 * The following headers are turned on by default:
	 * - dnsPrefetchControl (Controle browser DNS prefetching). https://helmetjs.github.io/docs/dns-prefetch-control
	 * - frameguard (prevent clickjacking). https://helmetjs.github.io/docs/frameguard
	 * - hidePoweredBy (remove the X-Powered-By header). https://helmetjs.github.io/docs/hide-powered-by
	 * - hsts (HTTP strict transport security). https://helmetjs.github.io/docs/hsts
	 * - ieNoOpen (sets X-Download-Options for IE8+). https://helmetjs.github.io/docs/ienoopen
	 * - noSniff (prevent clients from sniffing MIME type). https://helmetjs.github.io/docs/dont-sniff-mimetype
	 * - xssFilter (adds small XSS protections). https://helmetjs.github.io/docs/xss-filter/
	 */
	configureHelmet() {
		this.app.use(
			helmet({
				hsts: process.env.USE_HSTS,
			}),
		);

		return this;
	}

	connectToDatabase() {
		// this will need to change to a promise, so it can't return this
		// this may have to be async
		return Promise.resolve(this);
	}

	configurePublicPath(public_path = process.env.PUBLIC_DIRECTORY) {
		this.app.use(
			'/public',
			express.static(path.posix.join('bin', public_path)),
		);

		return this;
	}

	setPublicRoutes(routes = []) {
		routes.forEach(route => require(route)(this.app));
		return this;
	}

	setErrorRoutes() {
		return this;
	}

	listen(port = process.env.PORT, callback) {
		this.app.listen(port, callback);
		return this;
	}
}

module.exports = Server;
