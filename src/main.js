const getEnvironmentSettings = require('./config/env');
const compiler = require('./utils/webpack.utils');
const appConfig = require('./config/config');
const Server = require('./lib/server');
const { application: logger } = require('./lib/logger');

module.exports = function main(webpackConfig) {
	let { raw: env } = getEnvironmentSettings();
	let server = new Server()
		.configureMiddleware()
		.configureHelmet()
		.configureLocals(appConfig.locals)
		.configurePublicPath(env.PUBLIC_DIRECTORY)
		.configureViewEngine('pug', appConfig.files.views);

	// console.log(logger);

	// Compile our webpack assets
	logger.info('Compiling webpack assets');
	compiler
		.compile(webpackConfig)
		// Connect to our database if any
		.then(() => {
			logger.info('Connecting to database');
			return server.connectToDatabase();
		})
		// Save our stats somewhere and start the server
		.then(() => {
			logger.info('Setting up server routes');
			// Set up some custom middleware if we are in develop
			if (process.env.NODE_ENV === 'development') {
				let {
					webpackDevMiddleware,
					webpackHotMiddleware,
				} = compiler.middleware(webpackConfig);
				server.app.use(webpackDevMiddleware);
				server.app.use(webpackHotMiddleware);
			}
			// Set the public routes, spa route, error routes, and start the server
			server
				.setPublicRoutes(appConfig.files.routes)
				.setErrorRoutes()
				.listen(env.PORT, () =>
					logger.info(`Server listening on localhost:${env.PORT}`),
				);
		})
		// Catch any errors
		.catch(err => logger.error(err));
};
