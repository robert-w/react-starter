process.env.SERVER_SIDE_RENDERING = false;
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';
process.env.PORT = 3000;
process.traceDeprecation = true;

const path = require('path');

// Grab our webpack config
let config = require(path.posix.resolve('./src/config/webpack.config.dev'))();
let main = require(path.posix.resolve('./src/main'));

main(config);
