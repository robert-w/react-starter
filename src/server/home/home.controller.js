const { parseAssetPaths } = require('../../utils/webpack.utils');
const ReactDomServer = require('react-dom/server');
const config = require('../../config/config');
const React = require('react');
const path = require('path');
const fs = require('fs');

/**
 * @function home
 * @summary render the home page
 * @name exports.home
 * @static
 * @param {Express.Request} req - Express request object
 * @param {Express.Response} res - Express response object
 */
exports.home = (req, res) => {
	let stats = require(config.client.stats);
	let { cssPath, jsPath } = parseAssetPaths(stats.assetsByChunkName.home);
	let css = '',
		markup = '';

	if (cssPath) {
		let fullPath = path.posix.join(
			config.client.bin,
			stats.publicPath,
			cssPath,
		);
		css = fs.readFileSync(fullPath, { encoding: 'utf-8' });
	}

	// If we are on production, add in server rendering
	if (process.env.NODE_ENV === 'production') {
		let precompiled = require(config.client.precompiled);
		let fullPath = path.posix.join(
			precompiled.outputPath,
			precompiled.assetsByChunkName.home,
		);
		let component = require(fullPath);
		markup = ReactDomServer.renderToString(
			React.createElement(component.default),
		);
	}

	res.status(200).render('home', {
		commonjs: path.posix.join(stats.publicPath, stats.assetsByChunkName.common),
		mainjs: path.posix.join(stats.publicPath, jsPath),
		markup: markup,
		css: css,
	});
};
