const userController = require('../user/user.controller');
const controller = require('./home.controller');

/**
 * @name exports
 * @static
 * @summary Home page routes
 */
module.exports = app => {
	/**
	 * @name /home
	 * @see home.controller
	 * @memberof Router
	 * @description Render the home page
	 */
	app.get('/', userController.isAuthorized, controller.home);
};
