/**
 * @function signin
 * @summary Attempt to authenticate a user
 * @name exports.signin
 * @static
 * @param {Express.Request} req - Express request object
 * @param {Express.Response} res - Express response object
 */
exports.signin = (req, res) => {
	res.status(200).json({ status: 'success' });
};

/**
 * @function register
 * @summary Register a new user
 * @name exports.register
 * @static
 * @param {Express.Request} req - Express request object
 * @param {Express.Response} res - Express response object
 */
exports.register = (req, res) => {
	res.status(200).json({ status: 'success' });
};
