/**
 * @function isAuthorized
 * @summary Is the user logged in
 * @name exports.isAuthorized
 * @static
 * @param {Express.Request} req - Express request object
 * @param {Express.Response} res - Express response object
 * @param {Function} next - invoke the next middleware
 */
exports.isAuthorized = (req, res, next) => {
	if (req.isAuthenticated && req.isAuthenticated()) {
		next();
	} else {
		res.redirect('/auth/login');
	}
};
