const { ssrRenderPage } = require('../../../utils/server.utils.js');
const path = require('path');

/**
 * @function login
 * @summary render the login page
 * @name exports.login
 * @static
 * @param {Express.Request} req - Express request object
 * @param {Express.Response} res - Express response object
 */
exports.login = (req, res) => {
	let { material, markup, css, main, stats } = ssrRenderPage('login');

	res.status(200).render('login', {
		commonjs: path.posix.join(stats.publicPath, stats.assetsByChunkName.common),
		mainjs: path.posix.join(stats.publicPath, main),
		material: material,
		markup: markup,
		css: css,
	});
};
