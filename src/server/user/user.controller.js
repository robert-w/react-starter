const authentication = require('./controllers/authentication.controller.js');
const authorization = require('./controllers/authorization.controller.js');
const login = require('./controllers/login.controller.js');

/**
 * @name exports
 * @static
 * @summary All user related controller functions
 */
module.exports = {
	// Authorization related functions
	isAuthorized: authorization.isAuthorized,
	// Authentication related functions
	register: authentication.register,
	signin: authentication.signin,
	// Rendering related controller functions
	login: login.login,
};
