const userController = require('./user.controller.js');

/**
 * @name exports
 * @static
 * @summary Authentication routes
 */
module.exports = app => {
	/**
	 * @name /auth/login
	 * @see loginController
	 * @memberof Router
	 * @description Render the login page
	 */
	app.get('/auth/login', userController.login);

	/**
	 * @name /auth/signin
	 * @see authenticationController
	 * @memberof Router
	 * @description Attempt to signin the user
	 */
	app.post('/auth/signin', userController.signin);

	/**
	 * @name /auth/register
	 * @see authenticationController
	 * @memberof Router
	 * @description Attempt to signin the user
	 */
	app.post('/auth/register', userController.register);
};
