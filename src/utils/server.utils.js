const { createGenerateClassName } = require('@material-ui/core/styles');
const { parseAssetPaths } = require('./webpack.utils.js');
const { SheetsRegistry } = require('react-jss/lib/jss');
const ReactDomServer = require('react-dom/server');
const config = require('../config/config.js');
const React = require('react');
const path = require('path');
const fs = require('fs');

/**
 *
 */
exports.ssrRenderPage = function ssrRenderPage(chunkName) {
	let stats = require(config.client.stats);
	let material = '';
	let markup = '';
	let css = '';

	// Get the filepaths for our assets
	let { cssPath, jsPath: main } = parseAssetPaths(
		stats.assetsByChunkName[chunkName],
	);

	// If we have critical styles, read them in now
	if (cssPath) {
		let fullPath = path.posix.join(
			config.client.bin,
			stats.publicPath,
			cssPath,
		);
		css = fs.readFileSync(fullPath, { encoding: 'utf-8' });
	}

	// If we are on production, add in server rendering
	if (process.env.NODE_ENV === 'production') {
		let precompiled = require(config.client.precompiled);
		let fullPath = path.posix.join(
			precompiled.outputPath,
			precompiled.assetsByChunkName[chunkName],
		);
		let component = require(fullPath);
		// SSR things for Material UI Framework
		let generateClassName = createGenerateClassName();
		let registry = new SheetsRegistry();
		let manager = new Map();

		markup = ReactDomServer.renderToString(
			React.createElement(component.default, {
				generateClassName,
				registry,
				manager,
			}),
		);

		material = registry.toString();
	}

	return { material, markup, stats, main, css };
};
