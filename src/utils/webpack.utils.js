const webpackDevServerConfig = require('../config/webpack.dev-server.config');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const { application: logger } = require('../lib/logger');
const webpack = require('webpack');
const path = require('path');

let toStringOptions = {
	errorDetails: true,
	warnings: true,
	modules: false,
	chunks: false,
	colors: true,
};

function buildStaticAssets() {
	return new Promise((resolve, reject) => {
		logger.info('Pre-compiling our static assets');
		let webpackConfig = require(path.posix.resolve(
			'./src/config/webpack.config.precompile',
		))();
		let compiler = webpack(webpackConfig);

		compiler.run((err, stats) => {
			if (err) {
				return reject(err);
			}
			console.log('\n' + stats.toString(toStringOptions) + '\n');
			resolve();
		});
	});
}

/**
 * @description Run webpack to compile assets based on the provided config
 * we can bail in development mode because the webpack middleware handles
 * these things for us
 */
function compile(webpackConfig) {
	return new Promise((resolve, reject) => {
		// If we are in development mode, we don't need to pre-compile anything
		if (process.env.NODE_ENV === 'development') {
			return resolve();
		}
		// Precompile our static router
		return buildStaticAssets()
			.then(() => {
				// Compile our production assets
				let compiler = webpack(webpackConfig);
				logger.info('Compiling production assets');
				compiler.run((err, stats) => {
					if (err) {
						return reject(err);
					}
					console.log('\n' + stats.toString(toStringOptions) + '\n');
					resolve();
				});
			})
			.catch(reject);
	});
}

/**
 * @description Generate webpack middleware for express. Only use in development
 */
function middleware(webpackConfig) {
	let compiler = webpack(webpackConfig);
	return {
		webpackDevMiddleware: webpackDevMiddleware(
			compiler,
			webpackDevServerConfig,
		),
		webpackHotMiddleware: webpackHotMiddleware(compiler),
	};
}

/**
 * @description Parse assets from a stats.json entry. We use a plugin to write
 * out a stats file and the assetsByChunkName can either be an array or can be
 * a string. If it's an array, it contains the path to our critical css and js
 */
function parseAssetPaths(asset) {
	return Array.isArray(asset)
		? { cssPath: asset[0], jsPath: asset[1] }
		: { cssPath: '', jsPath: asset };
}

module.exports = {
	parseAssetPaths,
	middleware,
	compile,
};
